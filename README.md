# Gitlab ci files for ansible

### [ansible-playbook.yml](ansible-playbook.yml)

#### .ansible-playbook
Step for ansible-playbook with installing dependency from galaxy (optional)

#### Vars

| Var | Default value | Description |
| --- | --- | --- |
| ANSIBLE_IMAGE | lovtsovk/terraform_packer_ansible:3.0.0 | Docker image with ansible |
| ANSIBLE_ROOT | ansible | Root dir |
| ANSIBLE_PLAYBOOK | playbook.yml | ansible playbook to play |
| ANSIBLE_INVENTORY | inventory.yml | specify inventory host path or comma separated host list. |
| ANSIBLE_REQUIREMENTS_FILE | - | ansible-galaxy requirements file. Can be empty |
| ANSIBLE_PRIVATE_KEY | - | use this file to authenticate the connection |
| ANSIBLE_FORCE_COLOR | true | This option forces color mode even when running without a TTY or the “nocolor” setting is True |
| ANSIBLE_CREATE_PYTHON_VENV | false | Create python venv for dependencies |
| ANSIBLE_INSTALL_PYTHON_DEP | false | Install python dependencies from requirements file |
| ANSIBLE_PYTHON_REQUIREMENTS_FILE | requirements.txt | File with python dependencies |
